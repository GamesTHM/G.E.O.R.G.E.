# G.E.O.R.G.E.
In diesem Projekt werden alle Quellen bzgl. G.E.O.R.G.E. und Secret Science Society (3S) zusammengeführt.

## Projekte
Zu G.E.O.R.G.E. gehören die folgenden Projekte:
- [3S-Web](https://gitlab.com/GamesTHM/3S-Web)
- [3S-Android](https://gitlab.com/GamesTHM/3S-Android)
- [3S-IOS](https://gitlab.com/GamesTHM/3S-IOS)
- [3S-Windows (zzt. noch leer)](https://gitlab.com/GamesTHM/3S-Windows)
- [3S (Android nativ, veraltet)](https://gitlab.com/GamesTHM/3S)
- [G.E.O.R.G.E.-Admin-Tool](https://gitlab.com/GamesTHM/G.E.O.R.G.E.-Admin-Tool)
- 3S-Content (Texte, Grafiken, Bilder, etc., zzt. noch ohne Gitlab Projekt!)
- MongoDB für Parse-Daten (zzt. noch ohne Gitlab Projekt und Dokumentation!)
- Parse-Server (zzt. noch ohne Gitlab Projekt und Dokumentation!)
- Cloud Code (zzt. noch ohne Gitlab Projekt und Dokumentation!)
- Parse-Dashboard (zzt. noch ohne Gitlab Projekt und Dokumentation!)
- [OSM-Kartenserver](https://gitlab.com/GamesTHM/OSM-Kartenserver)

**ToDo**: Beschreibung der einzelnen Projekte.

### Management
Zzt. wird das Issue-Tracking-System im 3S-Web-Projekt verwendet. 
Dort sollten allerdings nur Issues erstellt werden, die zu diesem Projekt gehören.
Issues zu anderen Projekten sollten im jeweiligen Gitlab Projekt erstellt werden.

### Dokumentation
Zzt. gibt es wenig Dokumentation zu den einzelnen Projekten.
Es sollten zu jeden Projekt Readmes erstellt werden, in denen steht, wie man die Projekte installieren und weiterentwickeln kann und ggf. wo welche Dateien auf dem Server liegen.
Eventuell sollten zu den Projekten auch Wikis erstellt werden oder man erstellt ein gemeinsames Wiki in diesem Projekt.

## Ressourcen
Die Ressourcen des G.E.O.R.G.E.-Projekts befinden sich an folgenden Orten:
- [Google Drive Ordner (Beacon-, Quest- und Storydaten, das GDD, etc.)](https://drive.google.com/drive/folders/0B4AEjTvC7vYMSC1BUVhqUzd5MVU)
- [Moodle Kurs (EP-Unterlagen, Wiki, Dokumentation zur nativen Android Version, etc.)](https://moodle.thm.de/course/view.php?id=3242)
- NAS (Grafiken, Bilder, etc.)

Die Ressourcen sollten am besten an einer zentralen Stelle gesammelt werden.

